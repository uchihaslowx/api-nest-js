import pgStructure from 'pg-structure';
import { environment } from '../../../src/environmen/environmen';
import { FolderShema, FolderTable } from '../shared/ts-create/create-folders';
import * as chalk from 'chalk';
import { SharedFunctions } from '../shared/shared-functions/shared-functions';
import { Entity } from '../shared/ts-create/create-entitys';
import { Middleware } from '../shared/ts-create/create-middleware';
import { SubModulo } from '../shared/ts-create/create-sub-modulos';
import { Service } from '../shared/ts-create/create-service';
import { Controller } from '../shared/ts-create/create-controller';
import { Router } from '../shared/ts-create/create-router';
import { Module } from '../shared/ts-create/create-module';

export class OnCreateShema {
  public shFn = new SharedFunctions();

  constructor(public schema: string) {
    this.OnpgStructure(schema);
  }

  async OnpgStructure(schema: string) {
    const db = await pgStructure(environment, {});

    try {
      db.schemas.get(schema).tables;
      this.onCreateContend(db, schema);
    } catch (error) {
      console.log(
        chalk.bold.green('x') + chalk.bold.red(error) + chalk.bold.green('x'),
      );
    }
  }

  onCreateContend(db: any, schema: string) {
    new FolderShema(schema);
    const tables = db.schemas.get(schema).tables;
    const nameTables = [];
    for (const table of tables) {
      new FolderTable(schema, table.name);
      new Entity(schema, table);
      new Middleware(schema, table.name);
      new SubModulo(schema, table.name);
      new Service(schema, table.name);
      new Controller(schema, table.name);
      nameTables.push({
        path: this.shFn.replace(table.name),
        name: this.shFn.namePrimaryMayus(table.name),
      });
    }

    new Router(schema, nameTables);
    new Module(schema, nameTables)
  }
}
