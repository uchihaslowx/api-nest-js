import { prompt } from 'inquirer';
import { OnDatabase } from './actions/database';
import { OnCreateShema } from './actions/shema';

export class Generate {
  constructor() {
    this.promptS();
  }

  promptS() {
    prompt([
      {
        type: 'list',
        name: 'accion',
        message: 'Crear estructura a partir de',
        choices: ['1.) Base de datos', '2.) Esquema', '3.) Tabla'],
      },
    ]).then(answer => {
      this.onStructureModel(answer.accion);
    });
  }

  onStructureModel(schema: string) {
    const accion = Number(schema[0]);
    switch (accion) {
      case 1:
        this.OncreateDatabase()
        break;
      case 2:
        this.onCreateShema();
        break;
      default:
        break;
    }
  }

  private onCreateShema() {
    prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Nombre del esquema?',
      },
    ]).then(answer => {
      new OnCreateShema(answer.name);
    });
  }

  private OncreateDatabase() {
    new OnDatabase()
  }
}

new Generate();
