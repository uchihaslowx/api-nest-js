import * as fs from 'fs';

export class SharedFunctions {
  primaryMayus(name: string) {
    const _name = name.split('_');
    let newString = '';
    _name.forEach(element => {
      newString += element.charAt(0).toUpperCase() + element.slice(1);
    });
    return newString;
  }

  replace(name: string) {
    const regex = /_/gi;
    const newString = name.replace(regex, '-');
    return newString;
  }

  singularword(name: string) {
    const array = name.split('_');
    array.forEach((element, i) => {
      if (!element.split('es').pop()) {
        return (array[i] = element.substring(0, element.length - 2));
      }

      if (!element.split('s').pop()) {
        return (array[i] = element.substring(0, element.length - 1));
      }
    });
    return array.join('_');
  }

  namePrimaryMayus(name: string) {
    const _name = name.split('_');
    let newString = '';
    _name.forEach(element => {
      newString += element.charAt(0).toUpperCase() + element.slice(1);
    });

    return newString;
  }

  getTemplate(templateName: string) {
    const path = `util/generate/shared/templates/${templateName}.html`;
    const template = fs.readFileSync(path, 'utf8');
    return template;
  }

  verifyFolderExists(path) {
    if (fs.existsSync(`${path}`)) {
      return true;
    }

    return false;
  }
}
