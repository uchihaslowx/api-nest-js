import { Sequelize } from 'sequelize-typescript';
import { environment } from 'src/environmen/environmen';

export const MODELS = _getModels();

function _getModels(path = 'dist') {
  const models = [];
  const fs = require('fs');
  const dirs: string[] = fs.readdirSync(path);

  for (const dir of dirs) {
    const newPath = `${path}/${dir}`;
    const isDirectory = fs.lstatSync(newPath).isDirectory(); 
    if (isDirectory) {
      models.push(..._getModels(newPath));
    } else {
      const isEntity = dir.includes('entity.js');
      const path = require('path');
      newPath.replace('src', 'dist');
      newPath.replace('ts', 'js');
      const dirPath = path.join(process.cwd(), newPath);
      
      if (isEntity && !dir.includes('.map')) {
        const model = require(dirPath);
        for (const instance in model) {
          if (model.hasOwnProperty(instance)) {
            models.push(model[instance]);
          }
        }
      }
    }
  }

  return models;
}

const constants = environment;

export const sequelizeProvider = {
  provide: 'Sequelize',
  useFactory: async () => {
    const sequelize = new Sequelize({
      dialect: 'postgres',
      ...constants,
      models: MODELS,
    });

    return sequelize;
  },
};
