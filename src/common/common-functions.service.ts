import { Injectable } from '@nestjs/common';

@Injectable()
export class CommonFunctionsService {
  private _model: any;

  public set model(model: any) {
    this._model = model;
  }

  async findAll(params: any) {
    return await this._model.findAll(params);
  }

  async finAllCountAll(params: any) {
    return await this._model.findAndCountAll(params);
  }

  async findByPk(id: number, params: any) {
    return await this._model.findByPk(id, params);
  }

  async findOne(params: any) {
    return await this._model.findOne(params);
  }

  async findList(params: any) {
    return await this._model.findAll(params).map((i: any) => i.toJSON());
  }

  async create(data, params: any) {
    return await this._model.create(data, params);
  }

  async createMultipe(data: any, params: any): Promise<any> {
    return await this._model.bulkCreate(data, { returning: true, params });
  }

  async update(data: any, params: any) {
    params.where = { id: data.id };
    params.returning = true;
    params.plain = true;

    const result = await this._model.update(data, params);
    return result[1];
  }

  async delete(id: number) {
    return await this._model.destroy({ where: { id } });
  }
}
